what does it do?
flush_dns.ps1 remotely runs "ipconfig /flushdns" on a list of computers.

skills used:
> powershell
> PsTools

requirements:
> powershell
> PsTools

how to run:
1. paste machine names in machines_to_flush.txt
2. run flush_dns.ps1
