﻿# get computer names
$computer_names = Get-Content .\machines_to_flush.txt

# flush dns on the list of computers
foreach($computer in $computer_names) {
  Write-Host $computer
  .\PSTools\PsExec.exe \\$computer ipconfig /flushdns
}

Write-Host "done flushing"
